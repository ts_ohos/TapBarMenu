# Changelog
本组件基于原库master1.0.6版本移植

### v.1.0.0
移植OHOS后首次上传

support
帮助创建"点击栏"菜单布局的简单库。

not support
无

## Release Notes
<br>1.0.6 - Update Android dependencies and migrate to JitPack
<br>1.0.5 - Added Regular Drawable support
<br>1.0.4 - Memory optimizations, onClick listener fixes
<br>1.0.2 - You can now add custom icon via XML or code. See attributes.
<br>1.0.1 - Pre 21 SDK bugfixes
<br>1.0.0 - Initial release
