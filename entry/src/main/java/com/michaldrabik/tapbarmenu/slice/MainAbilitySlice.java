package com.michaldrabik.tapbarmenu.slice;

import com.michaldrabik.tapbarmenu.ResourceTable;
import com.michaldrabik.tapbarmenulib.TapBarMenu;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbilitySlice extends AbilitySlice {
  static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00101, "MainAbilitySlice");

  TapBarMenu tapBarMenu;

  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_main);

    tapBarMenu = (TapBarMenu) findComponentById(ResourceTable.Id_tapBarMenu);
    tapBarMenu.setClickedListener(component -> tapBarMenu.toggle());

    Component item1 = findComponentById(ResourceTable.Id_item1);
    item1.setClickedListener(this::onItemClick);

    Component item2 = findComponentById(ResourceTable.Id_item2);
    item2.setClickedListener(this::onItemClick);

    Component item3 = findComponentById(ResourceTable.Id_item3);
    item3.setClickedListener(this::onItemClick);

    Component item4 = findComponentById(ResourceTable.Id_item4);
    item4.setClickedListener(this::onItemClick);
  }

  private void onItemClick(Component component) {
    switch (component.getId()) {
      case ResourceTable.Id_item1:
        HiLog.info(MainAbilitySlice.label, "Item1!");
        break;
      case ResourceTable.Id_item2:
        HiLog.info(MainAbilitySlice.label, "Item2!");
        break;
      case ResourceTable.Id_item3:
        HiLog.info(MainAbilitySlice.label, "Item3!");
        break;
      case ResourceTable.Id_item4:
        HiLog.info(MainAbilitySlice.label, "Item4!");
        break;
      default:
        break;
    }
  }

  @Override
  public void onActive() {
    super.onActive();
  }

  @Override
  public void onForeground(Intent intent) {
    super.onForeground(intent);
  }
}
