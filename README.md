# TapBarMenu

本项目是基于开源项目TapBarMenu进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/michaldrabik/TapBarMenu)

移植版本：源master1.0.6版本

## 项目介绍
### 项目名称：TapBarMenu
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    帮助创建"点击栏"菜单布局的简单库。

### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/michaldrabik/TapBarMenu
### 编程语言：java

### 项目截图（涉及文件仅供demo测试使用）

![alt text](http://i.giphy.com/ZRCzrySXUaMwM.gif "Demo 1")

![运行效果](art/demo.gif)

## 安装教程

#### 方案一  
 
可以先下载项目，将项目中的library库提取出来放在所需项目中通过build配置
```Java
dependencies {
     implementation project(":library")
}
```

#### 方案二

- 1.项目根目录的build.gradle中的repositories添加：
```groovy
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
- 2.开发者在自己的项目中添加依赖
```groovy
dependencies {
    implementation 'com.gitee.ts_ohos:tapbarmenulib:1.0.0'
}
```

# How to use

Check sample project for a full example.

TapBarMenu is an extension of a LinearLayout so you can simply put it in your XML. For example:

    <com.michaldrabik.tapbarmenulib.TapBarMenu
                ohos:id="$+id:tapBarMenu"
                ohos:height="56vp"
                ohos:width="match_parent"
                ohos:orientation="horizontal"
                ohos:layout_alignment="bottom"
                ohos:bottom_margin="24vp"
                app:tbm_buttonPosition="1"
                app:tbm_menuAnchor="3">
    
                <Image
                    ohos:id="$+id:item1"
                    ohos:height="match_parent"
                    ohos:width="0vp"
                    ohos:weight="1"
                    ohos:top_padding="10vp"
                    ohos:bottom_padding="10vp"
                    ohos:image_src="$media:ic_person" />
    
                <Image
                    ohos:id="$+id:item2"
                    ohos:height="match_parent"
                    ohos:width="0vp"
                    ohos:weight="1"
                    ohos:top_padding="10vp"
                    ohos:bottom_padding="10vp"
                    ohos:image_src="$media:ic_location" />
    
                <Component
                    ohos:height="match_parent"
                    ohos:width="0vp"
                    ohos:weight="1"
                    />
    
                <Image
                    ohos:id="$+id:item3"
                    ohos:height="match_parent"
                    ohos:width="0vp"
                    ohos:weight="1"
                    ohos:top_padding="10vp"
                    ohos:bottom_padding="10vp"
                    ohos:image_src="$media:ic_thumb_up" />
    
                <Image
                    ohos:id="$+id:item4"
                    ohos:height="match_parent"
                    ohos:width="0vp"
                    ohos:weight="1"
                    ohos:top_padding="10vp"
                    ohos:bottom_padding="10vp"
                    ohos:image_src="$media:ic_thumb_down" />
    
            </com.michaldrabik.tapbarmenulib.TapBarMenu>

## License

    Copyright (C) 2019 Michal Drabik

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
